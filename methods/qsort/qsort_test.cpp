#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int compare(const void * a, const void * b){
	return (*(int*)a - *(int*)b);
}

int main(){
	srand(time(NULL));
	const int SIZE = 100000;
	int values[SIZE];

	for(int i = 0; i < SIZE; i++){
		values[i] = rand()%SIZE + 1;
	}

	clock_t t = clock();

	qsort(values, sizeof(values)/sizeof(values[0]), sizeof(values[0]), compare);

	float finish = ( ((float)clock() - (float)t)/CLOCKS_PER_SEC);

	std::cout << "Time: " << finish << std::endl;

	for(int i = 0; i < sizeof(values)/sizeof(values[0]); i++){
		std::cout << values[i] << std::endl;
	}



	return 0;
}