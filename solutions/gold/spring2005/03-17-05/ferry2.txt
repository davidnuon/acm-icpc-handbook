From swyrzyko@cecs.csulb.edu Thu Mar 17 21:24:45 2005
Date: Thu, 17 Mar 2005 21:20:58 -0800
From: Sylwester Wyrzykowski <swyrzyko@cecs.csulb.edu>
To: progcont@cecs.csulb.edu
Subject: Submit: 2

/*********************
Team: swyrzyko
Time: 1111123258
File: ferry.cpp
Problem: 2
*********************/
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{
        int cases, n, t, m, load = 0, arrive;
        int totTime =0, trips=0;

        vector<int> vec;
       
        cin >> cases;
       
        for(int i = 0; i < cases; i++)
        {
                cin >> n >> t >> m;

                for( int j = 0; j < m; j++ )
                {
                        cin >> arrive;

                        vec.push_back(arrive);
                       
                        ++load;

                        if(load == n || j == m-1)
                        {
                                ++trips;
                        //      totTime += 2 * t;
                                load = 0;

                        }
                }

        //      cout << "start" << endl;
        //      for( int i = 0; i < vec.size(); i++)
        //              cout << vec[i] << " " ;
        //      cout << endl;


                vector<int>:: iterator p;
               
                while( !vec.empty() )
                {
                        if( vec.size()% n == 0)
                        {
                                if( vec[n-1]+2*t > vec[vec.size() - 1])
                                {
                                         vec[vec.size() - 1] = vec[n-1] +2*t;
                                //      cout << "increm " << vec[n-1] << endl;
                                }

                                p = vec.begin();

                                vec.erase(p = vec.begin(), p=p+n);

                                if( vec.size() == n )
                                {
                                        cout << vec[vec.size()-1] + t << " " <<trips << endl;
                                        vec.clear();
                                }
/*                              for( int i = 0; i < vec.size(); i++)
                                        cout << vec[i] << " " ;
                                cout << endl;
*/
                               
                        }
                        else
                        {
                                int b = vec.size()% n;
                               
                                if( vec[b-1]+2*t > vec[vec.size() - 1])
                                {
                                         vec[vec.size() - 1] = vec[b-1] +2*t;
                                //      cout << "increm " << vec[b-1] << endl;
                                }

                                p = vec.begin();

                                vec.erase(p = vec.begin(), p=p+b);

                                if( vec.size() == n )
                                {
                                        cout << vec[vec.size()-1] + t << " " << trips << endl;
                                        vec.clear();
                                }
/*                              for( int i = 0; i < vec.size(); i++)
                                        cout << vec[i] << " " ;
                                cout << endl;
*/
                        }
                }

//              cout << arrive + t << " " << trips << endl;

                totTime = 0;
                trips = 0;
        }
        return 0;
}


