/*From zcsteele@cecs.csulb.edu Thu Mar  4 21:41:43 2004 
Date: Thu, 4 Mar 2004 21:35:43 -0800 
From: Zachary Steele <zcsteele@cecs.csulb.edu> 
To: progcont@cecs.csulb.edu 
Subject: Submit: 2 
*/ 
/********************* 
Team: zcsteele 
Time: 1078464943 
File: rev2.cpp 
Problem: 2 
*********************/ 
#include <iostream> 
#include <string.h> 
#include <vector> 
#include <iterator> 
#include <algorithm> 

using namespace std; 

int main(void){ 
        int count; 
        char num1[201], num2[201], output[202], disp[202]; 
        int val1, val2, carry=0, len1, len2, out, i; 
        int zeros=0; 

        for(cin >> count; count>0; --count){ 
                cin >> num1 >> num2; 

                len1 = strlen(num1); 
                len2 = strlen(num2); 
        
        
                for(i=0; (i<len1 || i<len2) ; i++){ 
                        val1 = (i<len1) ? (num1[i]-'0') : 0; 
                        val2 = (i<len2) ? (num2[i]-'0') : 0; 
                        out = val1 + val2 + carry; 
//                      cout << num1 << " " << num2 << endl; 
//                      cout << val1 << " " << val2 << " " << out << endl << endl; 
                        if(out > 9){ 
                                out -= 10; 
                                carry = 1; 
                        }else carry = 0; 
                        output[i] = out + '0'; 
//                      cout << "out=" << out << " " << out+'0' << endl; 
                } 
                if(carry == 1) output[i++]='1'; 
                output[i]='\0'; 
                
                for(zeros=0; output[zeros] == '0'; zeros++); 
                
                strcpy(disp, output+zeros); 
                cout << disp << endl; 
        } 
        
        return 0; 
} 

