/*
	If given a list of numbers like:

	1234
	1234
	1234
	1234
	1234
	1234-
	1234
	1234
	1234

	here's how to scan them in.
	NOTE: they have to fit into int or unsigned int
*/

#include <iostream>
using namespace std;

void calc(int n);

int main() {
	int number;
	while(cin >> number) calc(number);
}

void calc(int n) {
	cout << (n*n)/2 << endl;
}