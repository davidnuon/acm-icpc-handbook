From kharness@cecs.csulb.edu Thu Apr 21 21:07:00 2005
Date: Thu, 21 Apr 2005 20:45:18 -0700
From: Kevin Harness <kharness@cecs.csulb.edu>
To: progcont@cecs.csulb.edu
Subject: Submit: 1

/*********************
Team: kharness
Time: 1114141518
File: test.cpp
Problem: 1
*********************/
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

int ans[] = {
0,
0,
0,
1,
3,
8,
20,
47,
107,
238,
520,
1121,
2391,
5056,
10616,
22159,
46023,
95182,
196132,
402873,
825259,
1686408,
3438828,
6999071,
14221459,
28853662,
58462800,
118315137,
239186031,
483072832,
974791728
};

int main() {
	unsigned int i,max,mask,mmax,len,poss;
	string si,sf,n;
	while(cin>>len&&len) {
		cout << ans[len] << endl;
		continue;
		if(len<3) cout << 0 << endl;
		else {
			poss=0;
			max=1<<len;
			mmax=(7<<(len-2));
			for(i=0;i<max;i++) {
				for(mask=7;mask!=mmax;mask<<=1) {
					if(!(mask&i)) {
						poss++;
						break;
					}
				}
			}
			cout << poss << endl;
		}
	}
	return 0;
}

