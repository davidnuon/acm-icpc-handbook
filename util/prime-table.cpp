// Outputs a fixed-size C++ array of primes
// Useful when you have to look up primes over and over.

#include <iostream>

using namespace std;

const int length = 100; // Change the length here.
bool list[length];

bool isprime(int n) {
	if (n <  2) return false;
	if (n == 2) return true;
	for(int i = 2; i < n; i++) {
		if(n % i == 0) return false;
	}

	return true;
}

int main() {
	for(int i = 0; i < length; i++) {	
		list[i] = isprime(i);
	}
	
	string out = "bool primes[] = {";

	for(int i = 0; i < length; i++) {
		if(list[i]) out += "1";
		else        out += "0";
		
		if(i < length - 1) out += ",";
	}	

	out += "};";
	out += "\n";
	out += "int main() {}";
	
	cout << out << endl;
	return 0;
};
