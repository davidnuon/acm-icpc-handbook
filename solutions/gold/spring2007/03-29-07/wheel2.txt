From kharness@cecs.csulb.edu Thu Mar 29 20:21:49 2007
Date: Thu, 29 Mar 2007 20:21:48 -0700
From: Kevin Harness <kharness@cecs.csulb.edu>
To: progcont@cecs.csulb.edu
Subject: Submit: 2

/*********************
Team: kharness
Time: 1175224907
File: prob2.cpp
Problem: 2
*********************/
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <vector>
#include <sstream>
using namespace std;

bool matches(string pattern, string word, const char* guesses) {
	int i;
	if(pattern.size()!=word.size())
		return false;
	for(i=0;i<pattern.size();i++) {
		if(pattern[i]=='-') {
			if(NULL==strchr(guesses,word[i]))
				continue;
			return false;
		}
		if(pattern[i]!=word[i])
			return false;
	}
	return true;
}

void disp_results(const vector<vector<string> >& results, string prefix, int depth) {
	int i;
	if(depth>=results.size()) {
		cout << prefix << endl;
		return;
	}
	if(depth)
		prefix += ' ';
	for(i=0;i<results[depth].size();i++)
		disp_results(results,prefix+results[depth][i],depth+1);
}

int main() {
	vector<string> dict;
	int nwords;
	string line;
	getline(cin,line);
	istringstream input_num(line);
	input_num >> nwords;
	getline(cin,line);
	istringstream input_words(line);
	while(nwords--) {
		string w;
		input_words >> w;
		dict.push_back(w);
	}
	sort(dict.begin(),dict.end());
	while(getline(cin,line)) {
		istringstream get_patterns(line);
		string guesses = "";
		vector<string> pats;
		vector<vector<string> > results;
		string pat;
		while(get_patterns>>pat) {
			guesses += pat;
			pats.push_back(pat);
		}
		int i,j;
		for(j=0;j<pats.size();j++) {
			vector<string> mat;
			for(i=0;i<dict.size();i++) {
				if(matches(pats[j],dict[i],guesses.c_str()))
					mat.push_back(dict[i]);
			}
			results.push_back(mat);
		}
		disp_results(results,"",0);
	}
	
	return 0;
}
