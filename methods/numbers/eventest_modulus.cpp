#include <iostream>

using namespace std;
// 3 % 2 != 0 therefore odd
// 2 % 0 == 0 therefore even

int main() {
	unsigned int limit = 4294967295;

	for(unsigned int i = 0; i < limit; i++) {
		if(i % 2 != 0) continue;
	}
}