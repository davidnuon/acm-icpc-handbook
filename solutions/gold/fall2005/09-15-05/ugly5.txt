From rwilliam@cecs.csulb.edu Thu Sep 15 20:40:04 2005
Date: Thu, 15 Sep 2005 20:20:38 -0700
From: Ryan Williams <rwilliam@cecs.csulb.edu>
To: progcont@cecs.csulb.edu
Subject: Submit: 1

/*********************
Team: rwilliam
Time: 1126840838
File: contest1.cpp
Problem: 1
*********************/
//Ryan Williams
//CSULB PROGRAMMING TEAM
//program #1
//
#include <iostream>
#include <iomanip>

using namespace std;

void main()
{
  int count = 2; //the number of digits examined
  int ugly = 1; //the number of ugly numbers includes 1
  
  cout << "Looking for the 1500th ugly number...\n";
  do{
  	if(count % 2 == 0 || count % 3 == 0 || count % 5==0)
        {
		ugly++; //found another ugly number	 
 		count++; //to check the next number
	} //end if
        else
        count++;
  }while(ugly <1500);
 
  cout << "\n The 1500th ugly number is " << count << endl;

  	       

}//end main
