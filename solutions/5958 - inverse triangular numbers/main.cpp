#include <iostream>

using namespace std;

void invtriangle(int n) {
	int sum = 0;
	int sides = 0;
	int cur = 0;
	
	while(sum < n) {
		sum += (++cur);
		sides++;
	}
	
	if(sum == n) {
		cout << sides;
		return;
	}
	
	cout << "bad";
}

int main() {
	int tri;
	while(cin >> tri) {
		invtriangle(tri);
		cout << endl;
	}
}
