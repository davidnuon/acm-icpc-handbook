// 5989 - Here Be Dragons!
//
// https://icpcarchive.ecs.baylor.edu/index.php?option=com_onlinejudge&Itemid=8&category=544&page=show_problem&problem=4000

import java.io.BufferedReader;
import java.io.InputStreamReader;

class Main {

	public static void main(String[] args) throws Exception {

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		
		int numCases = Integer.parseInt(input.readLine().trim());
		
		while(numCases-- != 0){
			String s = input.readLine();
			if(s.contains("D"))
				System.out.println("You shall not pass!");
			else
				System.out.println("Possible");
				
		}
	}
}