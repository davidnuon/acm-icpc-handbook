/*********************
Team: lcohen
Time: 1094786104
File: hang.cpp
Problem: 2
*********************/
#include <iostream.h>

int main()
{
	float hang;
	int cards;
	float max[999];
	bool found;
	for(int i=1;i<999;i++)
	{
		max[i]=0;
		for(int j=2;j<i+2;j++)
		{
			max[i]+=1/float(j);
		}
	}

	cin>>hang;
	while(hang!=0.00)
	{
		found=false;
		for(int i=1;i<999;i++)
		{
			if(max[i]>hang&&!found)
			{
				found=true;
				cout<<i<<" card(s)\n";
			}
		}
		cin>>hang;
	}
	return 0;
}
	
