/*
3444 - Power Mean
Written by Shane
11/4/2012
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <cmath>

struct Series{
	double Power;
	std::vector<double> vec;
	double sum;
	double mean;
};

void parseData(Series &sir);
void printer(Series &sir);
void getPowerMean(Series &sir);
void printAnswer(Series &sir, bool &first);

int main(int argc, char* argv[]){
	bool first = true;
	double temp = 0;
	while(std::cin >> temp){
		Series sir;
		sir.Power = temp;
		sir.sum = 0;
		sir.mean = 0;

		parseData(sir);
		getPowerMean(sir);
		printAnswer(sir, first);
	}

}

void parseData(Series &sir){
	std::string line = "";
	getline(std::cin, line);

	std::string temp = "";
	for(int i = 0; i < line.size(); i++){
		if(line[i] >= '0' && line[i] <= '9' || line[i] == '.' || line[i] == '-'){
			temp += line[i];
		}
		else if( temp != "" ){
				sir.vec.push_back( atof( temp.c_str() ) );
				temp = "";
		}
	}
}

void printer(Series &sir){
	printf("%f ", sir.Power);
	for(int i = 0; i < sir.vec.size(); i++){
		printf("%.3f ", sir.vec[i]);
	}
	printf("%.3f", sir.mean);
	std::cout << std::endl;
}

void printAnswer(Series &sir, bool &first){
	if(!first){
		std::cout << " " << std::endl;
	}
	else{
		first = false;
	}
	printf("%.3f", sir.mean);
}

void getPowerMean(Series &sir){
	sir.sum = 0;
	for(int i = 0; i < sir.vec.size(); i++){
		sir.sum += pow(sir.vec[i], sir.Power);
	}
	sir.mean = sir.sum / sir.vec.size();
	sir.mean = pow(sir.mean, 1.0 / sir.Power);
}