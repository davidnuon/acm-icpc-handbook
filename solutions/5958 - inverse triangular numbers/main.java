// 5958 - Inverse Triangular Numbers
//
// https://icpcarchive.ecs.baylor.edu/index.php?option=com_onlinejudge&Itemid=8&category=542&page=show_problem&problem=3969

import java.io.BufferedReader;
import java.io.InputStreamReader;

class Main {

	public static void main(String[] args) throws Exception{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		try{
			while(true){
				int input = Integer.parseInt(br.readLine().trim());
				int i;
				for (i=1; input > 0; i++){
					input -= i;
				}
				if (input < 0)
					System.out.println("bad");
				else
					System.out.println(i-1);
			}
		}catch(Exception exception){
			return;
		}

	}

}
