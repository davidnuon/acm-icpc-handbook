#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <vector>

struct Lists{
	int maxLength;
	std::vector<double> vec;
	double mean;
	double median;
};

void parseList(Lists &currList);
void printer(Lists &currList);
int compare(const void * a, const void * b);
void findMM(Lists &currList);
void adder(Lists &currList);
double absolute(double num);

int main(int argc, char* argv[]){

	while(true){
		Lists currList;
		if(!(std::cin >> currList.maxLength)){
			break;
		}
		parseList(currList);
		//findMM(currList);
		//printer(currList);
		bool end = false;
		while(!end){
			qsort( (double*)&currList.vec[0], currList.vec.size(), sizeof(currList.vec[0]), compare );
			findMM(currList);
			//printer(currList);
			if( absolute(currList.mean - currList.median) < pow(10, -8) ){
				end = true;
			}
			adder(currList);
			if(currList.vec.size() > currList.maxLength){
				end = true;
			}
		}
		if(currList.vec.size() > currList.maxLength){
			printf(">%d\n", currList.maxLength);
		}
		else{
			printf("%d\n", currList.vec.size());
		}
	}

	return 0;
}

void parseList(Lists &currList){
	std::string line = "";
	getline(std::cin, line);
	std::string temp = "";

	for(int i = 0; i < line.size(); i++){
		if(line[i] >= '0' && line[i] <= '9' || line[i] == '.'){
			temp += line[i];
		}
		else if(line[i] == ' ' && temp != ""){
			double tempNum = atof(temp.c_str());
			currList.vec.push_back(tempNum);
			temp = "";
		}
	}
	if(temp != "" && temp != " "){
		double tempNum = atof(temp.c_str());
		currList.vec.push_back(tempNum);
		temp = "";
	}
}

void printer(Lists &currList){
	//std::cout << currList.maxLength << " ";
	for(int i = 0; i < currList.vec.size(); i++){
		//printf("%f ", currList.vec[i]);
		std::cout << currList.vec[i] << " ";
	}
	std::cout << std::endl << currList.mean << " " << currList.median;
	std::cout << std::endl;	
}

int compare(const void * a, const void * b){
	return ceil( *(double*)a - *(double*)b );
}

void findMM(Lists &currList){
	double sum = 0;
	int n = currList.vec.size();
	for(int i = 0; i < currList.vec.size(); i++){
		sum += currList.vec[i];
	}
	currList.mean = (double)sum / n;
	//printf("%f / %d = %f\n", sum, n, currList.mean);

	if(n % 2 == 0 && n != 0){
		currList.median = (currList.vec[n/2] + currList.vec[n/2 - 1]) / 2.f;
	}
	else{
		currList.median = currList.vec[n/2];
	}
}

void adder(Lists &currList){
	int n = currList.vec.size();
	double nextSum = currList.median * (n + 1);
	double nextNum = nextSum - (currList.mean * n);
	currList.vec.push_back(nextNum);
	currList.mean = currList.median;
}

double absolute(double num){
	if(num < 0)
		return num * -1;
	return num;
}