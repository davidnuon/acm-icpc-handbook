/*
	Sometimes you're given a table of values like this
	
	Bob Dannack|GA|1|2|3|4
	Sue Dannack|GA|1|2|3|4
	Ken Dannack|GA|1|2|3|4
	Dan Dannack|GA|1|2|3|4
	Steve      |GA|1|2|3|4

	here's how to scan them in.

	You can use strtok, but sometimes knowing another way is handy
	in case you forget how to use it.
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

struct line 
{
	string name;
	string state;
	int x, y, z, c;
};

// Function stub to test out our structs
void calc(line data) {
	cout << "Name: "  << data.name  << endl;
	cout << "State: " << data.state << endl;
	cout << "x: " << data.x << endl;
	cout << "y: " << data.y << endl;
	cout << "z: " << data.z << endl;
	cout << "c: " << data.c << endl;
}

int main() {
	// Storage
	line data; string buffy;
    const char DELIMITER = '|';

	// Read-in loop
	while(getline(cin, buffy))
	{
		string token = ""; int count = 1;
		for (int i = 0; i < buffy.length(); i++)
		{

			if(buffy[i] != DELIMITER) token += buffy[i]; 

			else {-
				/* 1st */ if(count % 6 == 1) data.name  = token;         
				/* 2nd */ if(count % 6 == 2) data.state = token; 
				/* 3rd */ if(count % 6 == 4) data.y = atoi(token.c_str()); 
				/* 4th */ if(count % 6 == 3) data.x = atoi(token.c_str()); 
				/* 5th */ if(count % 6 == 5) data.z = atoi(token.c_str()); 
				
				// We need to clear out token for the next scan
				// and increment count
				token = ""; count++;
			}
			
			// When we're at the end of the string, we have a token left
			/* last */ if(i == buffy.length() - 1) data.c = atoi(token.c_str()); 
		}
		calc(data);
 	}
}