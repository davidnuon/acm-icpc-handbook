/*From rnasr@cecs.csulb.edu Thu Mar 25 20:41:20 2004 
Date: Thu, 25 Mar 2004 20:23:03 -0800 
From: Ramzi Nasr <rnasr@cecs.csulb.edu> 
To: progcont@cecs.csulb.edu 
Subject: Submit: 2 
*/ 
/********************* 
Team: rnasr 
Time: 1080274983 
File: dungeon.cpp 
Problem: 2 
*********************/ 
#include<iostream> 
#include<vector> 
#include<queue> 
using namespace std; 

vector< vector< vector<char> > > board; 

struct move 
{ 
        enum { up, down, forward, backward, left, right, size }; 
}; 

struct point 
{ 
        int x,y,z; 
}; 

struct state 
{ 
        int x,y,z; 
        int minutes; 

        state(point P) 
                : x(P.x), y(P.y), z(P.z), minutes(0) {}; 

        bool isLegal(int i) 
        { 
                switch(i) 
                { 
                        case move::up: 
                                return  ((z-1) < 0) ? false : 
                                                        (board[x][y][z-1] != '#') ? true : false; 
                        case move::down: 
                                return  ((z+1) >= board[0][0].size()) ? false :         
                                                        (board[x][y][z+1] != '#') ? true : false; 
                        case move::forward: 
                                return  ((y-1) < 0) ? false : 
                                                        (board[x][y-1][z] != '#') ? true : false; 
                        case move::backward: 
                                return  ((y+1) >= board[0].size()) ? false : 
                                                        (board[x][y+1][z] != '#') ? true : false; 
                        case move::left: 
                                return  ((x-1) < 0) ? false : 
                                                        (board[x-1][y][z] != '#') ? true : false; 
                        case move::right: 
                                return  ((x+1) >= board.size()) ? false : 
                                                        (board[x+1][y][z] != '#') ? true : false; 
                } 
        } 

        state move(int i) 
        { 
                state rvl = *this; 

                switch(i) 
                { 
                        case move::up: 
                                if(board[x][y][z-1]!='E') board[x][y][z-1]='#'; 
                                rvl.z--; rvl.minutes++; 
                                break; 
                        case move::down: 
                                if(board[x][y][z+1]!='E') board[x][y][z+1]='#'; 
                                rvl.z++; rvl.minutes++; 
                                break; 
                        case move::forward: 
                                if(board[x][y-1][z]!='E') board[x][y-1][z]='#'; 
                                rvl.y--; rvl.minutes++; 
                                break; 
                        case move::backward: 
                                if(board[x][y+1][z]!='E') board[x][y+1][z]='#'; 
                                rvl.y++; rvl.minutes++; 
                                break; 
                        case move::left: 
                                if(board[x-1][y][z]!='E') board[x-1][y][z]='#'; 
                                rvl.x--; rvl.minutes++; 
                                break; 
                        case move::right: 
                                if(board[x+1][y][z]!='E') board[x+1][y][z]='#'; 
                                rvl.x++; rvl.minutes++; 
                                break; 
                } 

                return rvl; 
        } 
}; 
int main() 
{ 
        int x,y,z; 
        point S; 

        while(cin>>x>>y>>z) 
        { 
                if(x==0 && y==0 && z==0) 
                        break; 

                // resize the board 
                board.resize(x); 
                for(int i=0; i<board.size(); i++) 
                { 
                        board[i].resize(y); 
                        for(int j=0; j<board[i].size(); j++) 
                                board[i][j].resize(z); 
                } 

                // take the input 
                for(int i=0; i<x; i++) 
                for(int j=0; j<y; j++) 
                for(int k=0; k<z; k++) 
                { 
                        char ch; 
                        cin>>ch; 
                        board[i][j][k]=ch; 

                        if(ch=='S') S.x=i; 
                        if(ch=='S') S.y=j; 
                        if(ch=='S') S.z=k; 
                } 

                state stt = state(S); 
                queue<state> Q; 
                Q.push(stt); 

                while(!Q.empty()) 
                { 
                        stt = Q.front(); 
                        
                        if(board[stt.x][stt.y][stt.z]=='E') break; 



                        for(int i=0; i<move::size; i++) 
                                if(stt.isLegal(i)) 
                                        Q.push(stt.move(i)); 

                        Q.pop(); 
                } 

                if(Q.empty()) cout<<"Trapped!"<<endl; 
                else          cout<<"Escaped in " << stt.minutes 
                                       <<" minute(s)." << endl; 
        } 

        return 0; 
} 

