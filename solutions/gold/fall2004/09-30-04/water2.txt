Date: Thu, 30 Sep 2004 20:46:43 -0700
From: Carla Hernandez <chernand@cecs.csulb.edu>
To: progcont@cecs.csulb.edu
Subject: Submit: 1

/*********************
Team: chernand
Time: 1096602402
File: water.cpp
Problem: 1
*********************/
#include <iostream>
#include <vector>

struct pt {
	int	x0,y0;
	int	x1,y1;
	int	xd,yd;
	int	mn,md,b;
};

vector<pt> pts;

int main() {
	int NP;
	cin >> NP;
	pts.clear();
	for(int ln=0;ln<NP;ln++) {
		int x0,y0,x1,y1,mn,md,b;
		cin >> x0;
		cin >> y0;
		cin >> x1;
		cin >> y1;
		mn = y0-y1;
		md = x0-x1;
		b = md*y0-mn*x0;
		if(x0>x1) {
			x0 ^= x1;
			x1 ^= x0;
			x0 ^= x1;
			y0 ^= y1;
			y1 ^= y0;
			y0 ^= y1;
		}
		pt p;
		p.x0 = x0;
		p.y0 = y0;
		p.x1 = x1;
		p.y1 = y1;
		p.mn = mn;
		p.md = md;
		p.b = b;
		if(y0>y1) {
			p.xd = x1;
			p.yd = y1;
		} else {
			p.xd = x0;
			p.yd = y0;
		}
		pts.push_back(p);
	}
	int NS;
	cin >> NS;
	while(NS--) {
		int x,y;
		cin >> x;
		cin >> y;
		int iy;
		int bid,biy;
		while(y>0) {
			biy = 0;
			bid = -1;
			for(int i=0;i<NP;i++) {
				if(x<=pts[i].x1&&x>=pts[i].x0) {
					iy = (pts[i].mn*x+pts[i].b)/(pts[i].md);
					if(iy<y) {
						if(iy>biy) biy=iy,bid=i;
					}
				}
			}
			if(bid>=0) {
				x=pts[bid].xd;
				y=pts[bid].yd;
			} else y=0;
		}
		cout << x << endl;
	}
	return 0;
}
	
