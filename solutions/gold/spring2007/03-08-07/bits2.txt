From kharness@cecs.csulb.edu Thu Mar  8 19:56:06 2007
Date: Thu, 8 Mar 2007 21:56:05 -0600
From: Kevin Harness <kharness@cecs.csulb.edu>
To: progcont@cecs.csulb.edu
Subject: Submit: 1

/*********************
Team: kharness
Time: 1173412565
File: prob1.cpp
Problem: 1
*********************/
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
using namespace std;

void binout(int n) {
	int i;
	int mask = 0x1000;
	for(i=0;i<13;i++) {
		cout << ((n&mask)?'1':'0');
		mask >>= 1;
	}
}

int fromhex(string s) {
	return strtol(s.c_str(),NULL,16);
}

int main() {
	string line;
	getline(cin,line);
	istringstream Nis(line);
	int N;
	Nis >> N;
	while(N--) {
		getline(cin,line);
		string N1,op,N2;
		istringstream is(line);
		is >> N1 >> op >> N2;
		int a = fromhex(N1);
		int b = fromhex(N2);
		binout(a);
		cout << ' ' << op[0] << ' ';
		binout(b);
		cout << " = ";
		if(op[0]=='-')
			cout << a-b;
		else
			cout << a+b;
		cout << endl;
	}
	return 0;
}
