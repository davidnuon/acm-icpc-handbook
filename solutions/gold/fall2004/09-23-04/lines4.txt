#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;


vector<int>	x,y;
int	n;

int main()
{
	cin>>n;
	for (int i=0; i < n; i++)
	{
		int	a, b;
		cin>>a>>b;
		x.push_back(a);
		y.push_back(b);
	}

	int max = 0;
	for (int i=0; i < n; i++)
		for (int j=i+1; j < n; j++)
		{
			int	rise = y[j] - y[i],
					run = x[j] - x[i];
			int	tmp = 0;
			for (int k=0; k < n; k++)
			{
				int	rise1 = y[k] - y[i],
						run1 = x[k] - x[i];
				if ( rise*run1 == run*rise1 )	tmp++;
			}		
			if ( tmp > max )	max = tmp;
		}
	cout<<max<<endl;
	
	return 0;
}
