/*From ptwoo@cecs.csulb.edu Thu Mar 11 21:33:35 2004
Date: Thu, 11 Mar 2004 21:31:05 -0800
From: Paul Woo <ptwoo@cecs.csulb.edu>
To: progcont@cecs.csulb.edu
Subject: Submit: 2
*/
/*********************
Team: ptwoo
Time: 1079069465
File: sroman.cpp
Problem: 2
*********************/
#include <iostream>

using namespace std;

char romans[]={"IVXLCDMPQRSTUBWNYZ"};
int values[256];



void action1(char baseCode, int &number)
{
	cout << romans[baseCode];
	number-=values[romans[baseCode]];
}

void action2(char baseCode, int &number)
{
	cout << romans[baseCode-2] << romans[baseCode];
	number-=values[romans[baseCode]]-values[romans[baseCode-2]];
}

void action3(char baseCode, int &number)
{
	cout << romans[baseCode-1] << romans[baseCode];
	number-=values[romans[baseCode]]-values[romans[baseCode-1]];
}


int main()
{

	int scanNumber;
	char baseCode;

values['I']=1;
values['V']=5;
values['X']=10;
values['L']=50;
values['C']=100;
values['D']=500;
values['M']=1000;
values['P']=5000;
values['Q']=10000;
values['R']=50000;
values['S']=100000;
values['T']=500000;
values['U']=1000000;
values['B']=5000000;
values['W']=10000000;
values['N']=50000000;
values['Y']=100000000;
values['Z']=500000000;
	
	while(cin >> scanNumber)
	{
		if(!scanNumber)
			break;

		baseCode=17;

		while(scanNumber)
		{
//cout << scanNumber << ", " << baseCode << endl;
			if(baseCode%2==0)	// ones case
			{
				while(scanNumber>=values[romans[baseCode]])
					action1(baseCode,scanNumber);

				if(baseCode)
				{
					if(scanNumber >= values[romans[baseCode]]-values[romans[baseCode-2]])
						action2(baseCode,scanNumber);
				}
			}
			
			else	// fives case
			{
				if(scanNumber>=values[romans[baseCode]])
					action1(baseCode,scanNumber);

				if(scanNumber >= values[romans[baseCode]]-values[romans[baseCode-1]])
					action3(baseCode,scanNumber);
			}

			baseCode--;
			
		}

		cout << endl;
	}

	return 0;

}
