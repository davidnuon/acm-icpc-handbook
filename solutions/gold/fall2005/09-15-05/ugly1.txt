From hlina@cecs.csulb.edu Thu Sep 15 20:40:19 2005
Date: Thu, 15 Sep 2005 20:22:50 -0700
From: Hsiu-Chin Lin <hlina@cecs.csulb.edu>
To: progcont@cecs.csulb.edu
Subject: Submit: 1

/*********************
Team: hlina
Time: 1126840970
File: UglyNum.cpp
Problem: 1
*********************/
#include<iostream>
using namespace std;

bool isUglyNum( int num ) ;
int main ()
{
	int num = 4, i = 4;
	cout << "\nThe 1'st ugly number is 1."
	     << "\nThe 2'nd ugly number is 2."
	     << "\nThe 3'rd ugly number is 3." ;
	while ( i < 1501 )
	{
		if ( isUglyNum(num) )
		{
			cout << "\nThe " << i << "'th ugly number is " 
			     << num << ".";		
			i++ ;
		}
		num++ ;
	}
	cout << "\n\n";
	return 0 ;
}
bool isUglyNum( int num ) 
{
	while ( num % 2 == 0 ) num = num / 2 ;	
	while ( num % 3 == 0 ) num = num / 3 ;
	while ( num % 5 == 0 ) num = num / 5 ;
	return ( num == 1 ) ;
}
