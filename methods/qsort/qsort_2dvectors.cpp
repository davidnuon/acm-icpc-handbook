/*
	Sort 2D-Vectors represented as structs with qsort()
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// This is important
typedef int (*compfn)(const void*, const void*);
struct point { int x; int y; };

const int SIZE = 14;
struct point array[SIZE];

int compare(struct point *elem1, struct point *elem2)
{
	if (elem1->x == elem2->x && elem1->y == elem2->y) return 0;
	if (elem1->x < elem2->x)  return -1;
	if (elem1->x > elem2->x)  return 1;
	if (elem1->x == elem2->x && elem1->y < elem2->y) return -1;
	if (elem1->x == elem2->x && elem1->y > elem2->y) return 1;
}

void print()
{
   for (int i = 0; i < SIZE; i++) {
      printf("%d:  (%d, %d)\n", i+1, array[i].x, array[i].y);
   }
}

int main() {
   srand(time(NULL));
   for (int i = 0; i < SIZE; ++i) {
   		point p = {rand()%SIZE + 1, rand()%SIZE + 1};
   		array[i] = p;
   }

   /*
   qsort((void *) &array,              // Beginning address of array
   SIZE,                               // Number of elements in array
   sizeof(struct point),               // Size of each element
   (compfn)compare );                  // Pointer to compare function
   */

   qsort((void *) &array, SIZE, sizeof(struct point), (compfn)compare);
   print();
}
