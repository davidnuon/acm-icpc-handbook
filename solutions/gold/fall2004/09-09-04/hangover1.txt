/*********************
Team: hnguyenb
Time: 1094786270
File: hangover.cpp
Problem: 2
*********************/
#include <iostream>
using namespace std;

int main() {
	double f, l;
	int n;

	while (cin >> f, f != 0) {
		n = 2;
		l = 0;
		while (l += double(1)/n, l < f) {
			n++;
		}
		cout << n-1 << " card(s)" << endl;
	}
	 	  
	return 0;
}

