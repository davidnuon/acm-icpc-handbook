#include <iostream> 
#include <iomanip> 
#include <string> 

using namespace std; 

int main(void){ 
        string week[10]={ 
                " July 23", " July 30", " August 6", " August 13", " August 20", 
                " August 27", " September 3", " September 10", " September 17", 
                " September 24" 
        }; 
        string ss; 
        char s[2]; s[1]='\0'; 
        int fs, rw; 
        float i, l, c; 
        float r[10]; 
        int rc[10]; 

        for(int i=0; i<10; i++){ 
                r[i]=0; rc[i]=0; 
        } 

        cin >> ss; 
        while(ss != "000-00-0000"){ 
                cout << ss << "  $"; 
                cin >> fs >> i >> l; 
                switch(fs){ 
                case 1: case 3: c = 300; break; 
                case 4: c = 500;        break; 
                case 2: case 5: c = 600; 
                } 
                if((i/20) < c) c = (i/20); 
                if(l < c) c = l; 
                
                s[0] = ss[9]; 
                rw = atoi(s); 
                r[rw] += c; rc[rw]++; 

                cout << setprecision(2) << setiosflags(ios::fixed|ios::showpoint) 
                        << c << endl; 
                
                cin >> ss; 
        } 
        for(int i=0; i<10; i++){ 
                if(rc[i] != 0){ 
                        cout << rc[i] << "  $" << setprecision(2) 
                                << setiosflags(ios::fixed|ios::showpoint) 
                                << r[i] << week[i] << endl; 
                } 
        } 
        return 0; 
} 

