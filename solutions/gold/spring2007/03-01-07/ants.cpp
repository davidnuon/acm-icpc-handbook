#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int cases;
	int PipeLength;
	int AntCount;

	vector<int> pipe;
	vector<int>::iterator pos;

	cin >> cases;

	for(int i=0; i<cases; i++)
	{
		cin >> PipeLength >> AntCount;

		// initialize pipe
		pipe.clear();

		// position ants on pole;
		for(int k=0; k<AntCount; k++)
		{
			int antPos;
			cin >> antPos;
			pipe.push_back(antPos);
		}

		sort(pipe.begin(),pipe.end());	// put vector in order

		// find ant closest to pipe center to calculate minimum
		int pipe_center = PipeLength/2;
		int min_dist = PipeLength;
		int dist;

		for(pos=pipe.begin(); pos!=pipe.end(); ++pos)
		{
			if(*pos>pipe_center)
				dist=*pos-pipe_center;
			else
				dist=pipe_center-*pos;

			if(min_dist>dist)
				min_dist=dist;
		}

		int min=pipe_center-min_dist;
		
		// calculate distance end ants must walk for maximum
		int last = PipeLength-(*(pipe.end()-1));
		int max;

		if(*pipe.begin()<last)
			max=PipeLength-*pipe.begin();
		else
			max=PipeLength-last;

		// output result
		cout << min << " " << max << endl;

	}

	return 0;
}
