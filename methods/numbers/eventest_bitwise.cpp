#include <iostream>

using namespace std;
// 3 & 1 -> 011 & 001 = 001 therefore odd
// 2 & 1 -> 010 & 001 = 000 therefore even

int main() {
	unsigned int limit = 4294967295;

	for(unsigned int i = 0; i < limit; i++) {
		if(i & 1) continue;
	}
}