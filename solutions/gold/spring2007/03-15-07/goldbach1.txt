From kharness@cecs.csulb.edu Thu Mar 15 19:53:18 2007
Date: Thu, 15 Mar 2007 19:53:17 -0700
From: Kevin Harness <kharness@cecs.csulb.edu>
To: progcont@cecs.csulb.edu
Subject: Submit: 2

/*********************
Team: kharness
Time: 1174013596
File: prob2.cpp
Problem: 2
*********************/
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <vector>
using namespace std;

vector<bool> isprime;

void make_primes(int size) {
	isprime.clear();
	isprime.resize(size,true);
	isprime[0] = isprime[1] = false;
	int i,j;
	for(i=2;i<size;i++)
		if(isprime[i])
			for(j=2*i;j<size;j+=i)
				isprime[j] = false;
}

int main() {
	int N;
	make_primes(1<<20);
	while(cin>>N && N) {
		int i;
		int count = 0;
		for(i=2;i<=N/2;i++) {
			if(isprime[i] && isprime[N-i])
				count++;
		}
		cout << count << endl;
	}
	return 0;
}
