#include <iostream>

using namespace std;

int main() {
	int count = 0;
	const char DRAGON = 'D';
	string line;

	while(cin >> line) {
		bool possible = true;
		if (count > 0) {
			for(unsigned int k = 0; k < line.length(); k++){
				if(line[k] == DRAGON) {
					possible = false;
					break;				
				}
			}
			if (possible) {
				cout << "Possible" << endl;			
			} else {
				cout << "You shall not pass!" << endl;
			}			
		} 
		count++;
	}
}
