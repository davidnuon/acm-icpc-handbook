From hlina@cecs.csulb.edu Thu Oct 13 20:34:26 2005
Date: Thu, 13 Oct 2005 20:33:30 -0700
From: Hsiu-Chin Lin <hlina@cecs.csulb.edu>
To: progcont@cecs.csulb.edu
Subject: Submit: 2

/*********************
Team: hlina
Time: 1129260810
File: stat.cpp
Problem: 2
*********************/
#include <iostream>
#include <string>
#include <vector>
using namespace std ;

string input, key ;
bool read ;
vector<int> num ;
int ans ;

int sum ( vector<int> num ) 
{
	int s = 0 ;
	for ( int i = 0 ; i < num.size() ; i++ ) s += num[i] ;
	return s ;
}

int max( vector<int> num )
{
	int m = num[0] ;
	for ( int i = 1 ; i < num.size() ; i++ ) 
		if ( m < num[i] ) m = num[i] ;
	return m ;
}

int min ( vector<int> num )
{
	int n = num[0] ;
	for ( int i = 1 ; i < num.size() ; i++ )
		if ( n > num[i] ) n = num[i] ;
	return n ;
}

int main()
{
	cin >> key ;
	while ( !cin.eof() )
	{
		read = true ;
		num.clear() ;
		
		while ( !cin.eof() && read )
		{
			cin >> input ;
			if ( input[0] == '-' || (input[0] >='0' && input[0] <= '9' ) )
				num.push_back( atoi(input.c_str()) ) ;
			else read = false ;
		}
		
		if (key.compare("average") == 0 ) ans = sum(num) / num.size() ;
		else if (key.compare("sum") == 0 ) ans = sum(num) ;
		else if (key.compare ("maximum") == 0 ) ans = max (num ) ;
		else if (key.compare ("minimum" ) == 0 ) ans = min (num) ;
		else cout << "error\n" ;

		cout << ans << endl ;	
		key = input ;
	}
	return 0 ;
}
