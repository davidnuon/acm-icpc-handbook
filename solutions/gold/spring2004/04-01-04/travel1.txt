/*From zcsteele@cecs.csulb.edu Thu Apr  1 21:08:35 2004 
Date: Thu, 1 Apr 2004 21:05:22 -0800 
From: Zachary Steele <zcsteele@cecs.csulb.edu> 
To: progcont@cecs.csulb.edu 
Subject: Submit: 2 
*/ 

/********************* 
Team: zcsteele 
Time: 1080882322 
File: travel.cpp 
Problem: 2 
*********************/ 
#include <iostream> 

using namespace std; 



struct Light{ 
        float dist; 
        int g,y,r; 
        int total; 
}; 
int main(void){ 
        Light status[6]; 
        int delay[6]; 
        bool speed[30], pass, first; 
        int num_lights, velocity; 
        int time, cases=1, temp; 

        for(cin >> num_lights; num_lights > 0; cin >> num_lights){ 
                for(int i=0; i< 30; i++) speed[i]=true; 
                for(int i=0; i< num_lights; i++){ 
                        cin >> status[i].dist >> status[i].g >> status[i].y 
                                >> status[i].r; 
                        status[i].total = status[i].g + status[i].y + status[i].r; 
                } 
                for(velocity=30; velocity<=60; velocity++){ 
                        pass = true; 
                        for(int i=0; i<num_lights; i++){ 
                                time = static_cast<int>((status[i].dist/velocity)*3600)%status[i].total; 
//                              cout << time << ":"; 
                                time -= (status[i].g + status[i].y); 
//                              cout << time << "," << status[i].g << ":" << status[i].y << endl; 
                                if(time > 0 && time != status[i].r) pass = false; 
                        } 
                        speed[velocity-30] = pass; 
//                      cout << pass << ", "; 
                } 
//              for(int i=0; i<30; i++) cout << speed[i] << ", "; 
//              cout << endl; 
                velocity = 30; 
                first = true; 
                cout << "Case " << cases << ": "; 
                while(speed[velocity-30] == false && velocity<=60) velocity++; 
                if(velocity == 61) cout << "No acceptable speeds."; 
                else{ 
                        do{ 
                                if(!first)cout << ", "; 
                                first = false; 
                                cout << velocity++; 
                                temp = velocity; 
                                while(speed[velocity-30] == true && velocity<=60) velocity++; 
                                if(temp != velocity) cout << "-" << (velocity++)-1; 
                                while(speed[velocity-30] == false && velocity<=60) velocity++; 
                        }while(velocity <= 60); 
                } 
                cases++; 
                cout << endl; 
        } 

        return 0; 
} 

